package com.fulldife.rssviewer.dataClass;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.Collections;
import java.util.List;

@Root(strict = false)
public class Channel {
    @Element(name = "title")
    private String title;

    @Element(name = "description", data=true)
    private String description;

    @Element(name = "language")
    private String language;

    @ElementList(name = "items", required = false, inline = true)
    private List<Item> items;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Item> getItems() {
        return items == null ? Collections.EMPTY_LIST : items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "[title = " + title + ", description = " + description  + ", items = " + items
                + ", language = " + language + "]";
    }
}