package com.fulldife.rssviewer.dataClass;

import android.support.annotation.NonNull;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

@Root(strict = false)
public class Item implements Comparable<Item>, Serializable{
    public static final String SERIALIZED_NAME = "item";
    @Element(name = "pubDate")
    private String pubDate;
    @Element(name = "title")
    private String title;
    @ElementList(name = "content", inline = true, required = false)
    private List<Content> contents;
    @Element(name = "link")
    private String link;
    @Element(name = "description",required = false, data = true)
    private String description;

    private boolean favorite = false;

    public String getPubDate () {
        return pubDate;
    }

    public void setPubDate (String pubDate) {
        this.pubDate = pubDate;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public String getLink () {
        return link == null ? "" : link;
    }

    public void setLink (String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object obj) {
        return (((Item)obj).getLink().equals(this.getLink()));
    }

    @Override
    public int compareTo(@NonNull Item item) {
        //FIXME cast to Date
        return item.getPubDate().compareTo(this.getPubDate());
    }

    public String getImageURI() {
        try {
            return contents.get(0).getUrl();
        } catch (Exception e) {
            return getImageSource(getDescription()).trim();
        }
    }

    private String getImageSource(String string){
        if (string != null){
            try {
                int begin = string.indexOf("<img src=\"");
                int end =   string.indexOf("/>");
                return string.substring(begin, end).replace("<img src=\"", "")
                        .replace("/>", "").replace("\"","");
            }catch (Exception e){
                return "";
            }
        }
        return "";
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    private String getDescription() {
        return description == null? "" : description;
    }
}