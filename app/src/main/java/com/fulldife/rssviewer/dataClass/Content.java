package com.fulldife.rssviewer.dataClass;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root()
@Namespace(prefix = "media")
public class Content implements Serializable{
    @Attribute(name = "url", required = false)
    private String url;
    @Attribute(name = "medium", required = false)
    private String medium;
    @Attribute(name = "type", required = false)
    private String type;
    @Element(name = "title", required = false)
    private String title;

    String getUrl() {
        return url == null? "" : url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}