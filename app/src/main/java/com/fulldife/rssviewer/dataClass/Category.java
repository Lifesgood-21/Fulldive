package com.fulldife.rssviewer.dataClass;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Category {

    private String content;
    @Attribute(required = false)
    private String domain;

    public String getContent () {
        return content;
    }

    public void setContent (String content) {
        this.content = content;
    }

    public String getDomain () {
        return domain;
    }

    public void setDomain (String domain) {
        this.domain = domain;
    }

    @Override
    public String toString()
    {
        return "[content = "+content+", domain = "+domain+"]";
    }
}
