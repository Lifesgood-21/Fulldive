package com.fulldife.rssviewer;
import com.fulldife.rssviewer.dataClass.Rss;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface FulldiveApi {

    String BASE_URL = "https://lifehacker.com";
    String API_URL = BASE_URL + "/";

    @GET("https://lifehacker.com/rss/")
    Single<Rss> getLifehackerRss();

    @GET("http://feeds.feedburner.com/TechCrunch/")
    Single<Rss> getFeedburnerRss();
}