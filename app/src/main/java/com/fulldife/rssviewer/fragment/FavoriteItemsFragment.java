package com.fulldife.rssviewer.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fulldife.rssviewer.BuildConfig;
import com.fulldife.rssviewer.R;
import com.fulldife.rssviewer.acitivities.MainActivity;
import com.fulldife.rssviewer.adapters.ItemsRecyclerViewAdapter;

public class FavoriteItemsFragment extends Fragment {

    private DataUpdateBroadcastReceiver receiver;
    private RecyclerView itemsRecyclerView;
    private ItemsRecyclerViewAdapter itemsRecyclerViewAdapter;

    public FavoriteItemsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorite_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        itemsRecyclerView = view.findViewById(R.id.itemsRecyclerView);
        initCompanyRecyclerView();
    }

    private void initCompanyRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        itemsRecyclerView.setLayoutManager(layoutManager);
        itemsRecyclerViewAdapter = new ItemsRecyclerViewAdapter(
                (ItemsRecyclerViewAdapter.ItemClickListener) getActivity()
        );
        itemsRecyclerView.setAdapter(itemsRecyclerViewAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        receiver = new DataUpdateBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(
                BuildConfig.APPLICATION_ID + MainActivity.ACTION_NOTIFY
        );
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        getContext().registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (receiver != null){
            getContext().unregisterReceiver(receiver);
            receiver = null;
        }
    }

    private void updateData() {
        itemsRecyclerViewAdapter.replaceData(((MainActivity)getActivity()).getFavoriteItems());
    }

    public class DataUpdateBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(MainActivity.KEY_DATA_CHANGED) &&
                    (intent.getBooleanExtra(MainActivity.KEY_DATA_CHANGED, false))){
                updateData();
            }
        }
    }
}
