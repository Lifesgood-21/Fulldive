package com.fulldife.rssviewer;

import com.fulldife.rssviewer.acitivities.MainActivity;
import com.fulldife.rssviewer.dependencyModules.NetworkModule;
import javax.inject.Singleton;

import dagger.Component;

    @Component(modules = {NetworkModule.class})
    @Singleton
    public interface FulldiveAppcomponent {
        void inject(FulldiveApplication fulldiveApplication);
        void inject(MainActivity mainActivity);
    }
