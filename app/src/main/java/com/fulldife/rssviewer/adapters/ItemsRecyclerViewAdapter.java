package com.fulldife.rssviewer.adapters;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fulldife.rssviewer.R;
import com.fulldife.rssviewer.dataClass.Item;

import java.util.List;


public class ItemsRecyclerViewAdapter extends RecyclerView.Adapter<ItemsRecyclerViewAdapter.ItemViewHolder> {

    private SortedList<Item> items = new SortedList<>(Item.class,
            new SortedListAdapterCallback<Item>(this) {
        @Override
        public int compare(Item o1, Item o2) {
            return o2.getPubDate().compareTo(o1.getPubDate());
        }

        @Override
        public boolean areContentsTheSame(Item oldItem, Item newItem) {
            return oldItem.equals(newItem) && oldItem.isFavorite() == newItem.isFavorite();
        }

        @Override
        public boolean areItemsTheSame(Item item1, Item item2) {
            return item1.equals(item2);
        }
    });

    private ItemClickListener itemClickListener;

    public ItemsRecyclerViewAdapter(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    public void updateData(List<Item> newData){
        items.addAll(newData);
    }

    public void replaceData(List<Item> newData){
        items.clear();
        updateData(newData);
    }

    public interface ItemClickListener {
        void onItemClick(Item item);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemsRecyclerViewAdapter.ItemViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Item item = items.get(position);
        holder.bindTo(item, position);
    }

    @Override
    public int getItemCount() {
        return (items != null? items.size():0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView favoriteImageView;
        private int position;
        private TextView titleTextView;
        private TextView dateTextView;
        private ImageView image;
        private Item item;

        ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> itemClickListener.onItemClick(item));
            titleTextView = itemView.findViewById(R.id.titleTextView);
            dateTextView = itemView.findViewById(R.id.itemDateTextView);
            image = itemView.findViewById(R.id.imageDraweeView);
            favoriteImageView = itemView.findViewById(R.id.favoriteImageView);
        }

        void bindTo(Item item, int position) {
            this.item = item;
            this.position = position;
            titleTextView.setText(item.getTitle());
            dateTextView.setText(item.getPubDate());
            Glide.with(image).load(item.getImageURI()).into(image);
            favoriteImageView.setVisibility(item.isFavorite()? View.VISIBLE : View.GONE);
        }
    }
}