package com.fulldife.rssviewer.dependencyModules;


import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.fulldife.rssviewer.FulldiveApi;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class NetworkModule {

    public NetworkModule(Context context) {
        Context context1 = context;
    }

    @Provides
    @Singleton
    FulldiveApi provideApi(Retrofit retrofit) {
        return retrofit.create(FulldiveApi.class);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(FulldiveApi.API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(client)
                .build();
    }

//    @Provides
//    @Singleton
//    GsonBuilder provideGsonBuilder() {
//        return new GsonBuilder()
//                .serializeNulls()
//                .setDateFormat("dd.MM.yyyy HH:mm:ss")
//                //.setDateFormat(DateFormat.FULL, DateFormat.FULL)
//                .setLenient();
//    }

    @Provides
    @Singleton
    HostnameVerifier provideHostNameVerifier(){
        return (hostname, session) -> hostname.equals(
                FulldiveApi.BASE_URL.replace("https://", "")
        );
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HostnameVerifier verifier, SSLSocketFactory socketFactory, X509TrustManager trustManager){
        return new OkHttpClient.Builder()
                //.sslSocketFactory(socketFactory, trustManager)
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY)
                )
                //.hostnameVerifier(verifier)
                .build();
    }

    @Provides
    @Singleton
    SSLSocketFactory provideSslSocketFactory(X509TrustManager trustManager){
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (sslContext!=null)
            try {
                sslContext.init(null, new TrustManager[]{trustManager} ,new SecureRandom());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        return sslContext == null? null : sslContext.getSocketFactory();
    }

    @Provides
    @Singleton
    X509TrustManager provideTrustManager(){
        TrustManager[] trustAllCerts = new TrustManager[0];
        try {
            // Create a trust manager that does not validate certificate chains
            trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[0];
                        }
                    }
            };
        } catch(Exception e ){
            e.printStackTrace();
        }
        return (X509TrustManager) trustAllCerts[0];
    }
}
