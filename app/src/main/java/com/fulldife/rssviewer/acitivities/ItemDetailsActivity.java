package com.fulldife.rssviewer.acitivities;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fulldife.rssviewer.R;
import com.fulldife.rssviewer.dataClass.Item;

public class ItemDetailsActivity extends CustomActivity {

    private Item currentItem;
    private ImageView itemImageView;
    private TextView descriptionTextView;
    private MenuItem likeMenuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        handleIntent(getIntent());
        if (getActionBar()!=null)
            getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        setContentView(R.layout.activity_item_details);
        itemImageView = findViewById(R.id.itemImageView);
        descriptionTextView = findViewById(R.id.descriptionTextView);
    }


    private void handleIntent(Intent intent) {
        if (intent!= null && intent.hasExtra(Item.SERIALIZED_NAME)){
            currentItem = (Item) intent.getSerializableExtra(Item.SERIALIZED_NAME);
            showItem(currentItem);
        }
    }

    private void showItem(Item item) {
        Glide.with(this).load(item.getImageURI()).into(itemImageView);
        descriptionTextView.setText(item.getTitle());
        colorizeMenuItem(likeMenuItem, item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_like : {
                markItemAsFavorite(currentItem);
                break;
            }
            case R.id.action_open_in_browser : {
                openItemInBrowser(currentItem);
                break;
            }
            default:{
                Toast.makeText(this, getString(R.string.action_not_supported),
                        Toast.LENGTH_LONG
                ).show();
            }
        }
        return true;
    }

    private void markItemAsFavorite(Item item) {
        item.setFavorite(!item.isFavorite());
        showItem(item);
    }

    private void openItemInBrowser(Item item) {
        if (item != null && item.getLink()!= null){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getLink()));
            ComponentName componentName = intent.resolveActivity(getPackageManager());
            if (componentName!=null)
                startActivity(intent);
            else
                Toast.makeText(this, getString(R.string.cant_open_link), Toast.LENGTH_LONG).show();
        }else
            Toast.makeText(this, getString(R.string.link_is_empty), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_details_menu, menu);
        likeMenuItem = menu.findItem(R.id.action_like);
        colorizeMenuItem(likeMenuItem, currentItem);
        return true;
    }

    private void colorizeMenuItem(MenuItem menuItem, Item item) {
        if (item != null && menuItem != null){
            menuItem.getIcon().setColorFilter(getResources().getColor(
                    item.isFavorite()? R.color.colorAccent : R.color.milk), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Item.SERIALIZED_NAME, currentItem);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
