package com.fulldife.rssviewer.acitivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import com.fulldife.rssviewer.BuildConfig;
import com.fulldife.rssviewer.FulldiveApi;
import com.fulldife.rssviewer.FulldiveApplication;
import com.fulldife.rssviewer.R;
import com.fulldife.rssviewer.adapters.ItemsRecyclerViewAdapter;
import com.fulldife.rssviewer.dataClass.Item;
import com.fulldife.rssviewer.dataClass.Rss;
import com.fulldife.rssviewer.fragment.FavoriteItemsFragment;
import com.fulldife.rssviewer.fragment.ItemsFragment;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends CustomActivity implements ItemsRecyclerViewAdapter.ItemClickListener {
    public static final String KEY_DATA_CHANGED = "dataChanged";
    public static final String ACTION_NOTIFY = ".dataChangesOccur";
    private static final int ITEM_REQUEST_CODE = 123;
    @Inject
    FulldiveApi api;
    private Disposable lifehackerDisposable;
    private Disposable feedburnerDisposable;
    private ViewPager viewPager;
    private ItemsPagerAdapter adapter;
    private List<String> tabList = new ArrayList<String>(){};
    public List<Item> items = new ArrayList<Item>(){};
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FulldiveApplication.appComponent.inject(this);
        initViews();
        getRss();
    }

    private void initViews() {
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.loadProgressBar);
        tabList.add(getString(R.string.news));

        adapter = new ItemsPagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void getRss() {
        if (feedburnerDisposable == null || feedburnerDisposable.isDisposed())
            compositeDisposable.add(feedburnerDisposable = api.getFeedburnerRss()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> progressBar.setVisibility(View.VISIBLE))
                .doFinally(() -> {
                    if (lifehackerDisposable == null || lifehackerDisposable.isDisposed())
                        progressBar.setVisibility(View.INVISIBLE);
                })
                .subscribe(this::handleRssResponse, this::handleError)
            );

        if (lifehackerDisposable == null || lifehackerDisposable.isDisposed())
            compositeDisposable.add(lifehackerDisposable = api.getLifehackerRss()
                    .subscribeOn(Schedulers.io())
                    .delay(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> progressBar.setVisibility(View.VISIBLE))
                    .doFinally(() -> {
                        if (feedburnerDisposable == null || feedburnerDisposable.isDisposed())
                            progressBar.setVisibility(View.INVISIBLE);
                    })
                    .subscribe(this::handleRssResponse, this::handleError)
            );
    }

    private void handleRssResponse(Rss rss) {
        items.addAll(rss.getChannel().getItems());
        notifyFragmentsAboutItemsChanged();
        if (listContainsFavorite(items))
            addFavoriteTab();
        else
            removeFavoriteTab();
    }

    private void removeFavoriteTab() {
        if (tabList!= null){
            tabList.remove(getString(R.string.favorite));
            adapter.notifyDataSetChanged();
            viewPager.setCurrentItem(0);
        }
    }

    private void addFavoriteTab() {
        if (!tabList.contains(getString(R.string.favorite))){
            tabList.add(getString(R.string.favorite));
            adapter.notifyDataSetChanged();
        }
    }

    private void notifyFragmentsAboutItemsChanged() {
        Intent intent = new Intent();
        intent.setAction(BuildConfig.APPLICATION_ID + ACTION_NOTIFY);
        intent.putExtra(KEY_DATA_CHANGED,true);
        sendBroadcast(intent);
    }

    public List<Item> getFavoriteItems(){
        ArrayList tmpItems = new ArrayList<Item>();
        for (Item item : items)
            if (item.isFavorite())
                tmpItems.add(item);
        return tmpItems;
    }

    public boolean listContainsFavorite(List<Item> items){
        for (Item item : items)
            if (item.isFavorite())
                return true;
        return false;
    }

    private void handleError(Throwable throwable) {
        //TODO add displaying error
        throwable.printStackTrace();
    }

    @Override
    public void onItemClick(Item item){
        Intent intent = new Intent(this, ItemDetailsActivity.class);
        intent.putExtra(Item.SERIALIZED_NAME,  item);
        startActivityForResult(intent, ITEM_REQUEST_CODE);
    }

    private void handleResult(Intent data) {
        if (data.hasExtra(Item.SERIALIZED_NAME)){
            Item modifiedItem = (Item) data.getSerializableExtra(Item.SERIALIZED_NAME);
            replaceItem(items, modifiedItem);
        }
    }

    private void replaceItem(List<Item> items, Item modifiedItem) {
        items.remove(modifiedItem);
        items.add(modifiedItem);
        if (listContainsFavorite(items))
            addFavoriteTab();
        else
            removeFavoriteTab();
        notifyFragmentsAboutItemsChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ITEM_REQUEST_CODE :{
                if (resultCode == RESULT_OK)
                    handleResult(data);
                break;
            }
        }
    }

    private class ItemsPagerAdapter extends FragmentPagerAdapter {

        ItemsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 1:
                    return new FavoriteItemsFragment();
                default:
                    return new ItemsFragment();
            }
        }

        @Override
        public int getCount(){
            return tabList == null? 0 : tabList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList.get(position);
        }
    }
}
