package com.fulldife.rssviewer;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.fulldife.rssviewer.dependencyModules.NetworkModule;


public class FulldiveApplication extends Application {

    public static FulldiveAppcomponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
        appComponent.inject(this);
        initStetho();
    }

    private void initAppComponent() {
        appComponent = DaggerFulldiveAppcomponent
            .builder()
            .networkModule(new NetworkModule(this))
            .build();
    }

    private void initStetho(){
        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);
        //Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );
        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );
        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();
        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);
    }
}
